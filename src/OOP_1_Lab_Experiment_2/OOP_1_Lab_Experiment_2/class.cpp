#include <iostream>
#include <sstream>
using namespace std;

/*
Enter code for class Student here.
Read statement for specification.
*/

class Student {
private:

	int age;
	string first_name;
	string last_name;
	int standard;

public:
	// Set and get age
	void set_age(int a) {
		age = a;
	}
	int get_age() {
		return age;
	}
	// Set and get first name
	void set_first_name(string a) {
		first_name = a;
	}
	string get_first_name() {
		return first_name;
	}
	// Set and get last name
	void set_last_name(string a) {
		last_name = a;
	}
	string get_last_name() {
		return last_name;
	}
	// Set and get standard
	void set_standard(int a) {
		standard = a;
	}
	int get_standard() {
		return standard;
	}
	// To string method
	string to_string() {
		int age = get_age(), standard = get_standard();
		string name = get_first_name(), lastname = get_last_name();
		string outputstr;
		stringstream a, b;
		a << age;
		b << standard;
		string agestr = a.str();
		string stdstr = b.str();

		outputstr = agestr + "," + name + "," + lastname + "," + stdstr;

		return outputstr;
	}
};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	return 0;
}