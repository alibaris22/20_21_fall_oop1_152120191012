#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int n, q;
	cin >> n >> q;

	vector<int> arr[n];

	for (int i = 0; i < n; i++)
	{
		int j;
		cin >> j;
		int temp;

		for (int k = 0; k < j; k++) {
			cin >> temp;
			arr[i].push_back(temp);
		}
	}

	int x, y;
	for (int i = 0; i < q; i++) {
		cin >> x >> y;
		cout << arr[x][y] << endl;
	}


	return 0;
}