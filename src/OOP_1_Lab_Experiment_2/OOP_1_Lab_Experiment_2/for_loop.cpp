#include <iostream>
#include <cstdio>
using namespace std;

int main() {
	// Complete the code.
	int floor, ceil;
	cin >> floor >> ceil;

	for (int i = floor; i <= ceil; i++) {
		if (i <= 9 && i >= 1) {
			if (i == 1)
				cout << "one" << endl;
			if (i == 2)
				cout << "two" << endl;
			if (i == 3)
				cout << "three" << endl;
			if (i == 4)
				cout << "four" << endl;
			if (i == 5)
				cout << "five" << endl;
			if (i == 6)
				cout << "six" << endl;
			if (i == 7)
				cout << "seven" << endl;
			if (i == 8)
				cout << "eight" << endl;
			if (i == 9)
				cout << "nine" << endl;
		}
		else if (i > 9 && i % 2 == 1) {
			cout << "odd" << endl;
		}
		else if (i > 9 && i % 2 == 0) {
			cout << "even" << endl;
		}
	}
	return 0;
}