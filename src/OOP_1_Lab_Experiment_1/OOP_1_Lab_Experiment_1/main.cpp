#include<iostream>
#include<stdlib.h>
#include<fstream>

#define SIZE 100

using namespace std;

/// Sums all variables
///
/// Sums all variables and gives the sum as a return value
/// @param varcount Count of the variables
/// @param variables[] Array that stores the variables
/// @returns Sum of the variables
int sum(int varcount, int variables[]);

/// Multiplies all variables
///
/// Multiplies all variables and gives the product as a return value
/// @param varcount Count of the variables
/// @param variables[] Array that stores the variables
/// @returns Product of the variables
int product(int varcount, int variables[]);

/// Gets the average of all variables
///
/// Gets the average of all variables and gives it as a return value
/// @param varcount Count of the variables
/// @param variables[] Array that stores the variables
/// @returns Average of the variables
float average(int varcount, int variables[]);

/// Compares all variables
///
/// Compares all variables and returns the smallest one
/// @param varcount Count of the variables
/// @param variables[] Array that stores the variables
/// @returns Smallest one of the variables
int smallestNum(int varcount, int variables[]);

/// Prints all the outputs
///
/// Prints all the outputs as the wanted format
/// @param sum Sum of the variables
/// @param product Product of the variables
/// @param avg Average of the variables
/// @param smallest Smallest one of the variables
void outputs(int sum, int product, float avg, int smallest);

/// Checks errors
///
/// Checks the possible errors
/// @param varcount Count of the variables given by user
/// @param counter Counter of the real variable count
void errors(int varcount, int counter);

int main() {
	int variables[SIZE], varcount, varsum, varproduct, smallestvar, counter = 0;
	float varavg;

	// Opening "input.txt" in reading mode
	ifstream input;
	input.open("input.txt", ios::in);
	// If file cannot be opened, error message appears and program exits.
	if (input.is_open() != 1) {
		cout << "Your file cannot be opened." << endl;
		exit(1);
	}
	
	// Getting the first line variable as variable count
	input >> varcount;

	// Getting all variables from second line into 'variables' array
	while (!input.eof()) {
		input >> variables[counter];
		counter++;
	}

	errors(varcount, counter);

	// Calling sum function and storing return(sum) value in 'varsum'
	varsum = sum(varcount, variables);
	// Calling product function and storing return(product) value in 'varproduct'
	varproduct = product(varcount, variables);
	// Calling average function and storing return(average) value in 'varavg'
	varavg = average(varcount, variables);
	// Calling smallestNum function and storing return(smallest variable) value in 'smallestvar'
	smallestvar = smallestNum(varcount, variables);

	// Calling the outputs function to print all outputs
	outputs(varsum, varproduct, varavg, smallestvar);

	return 0;
}

int sum(int varcount, int variables[]) {
	// Initializing default sum value
	int sum = 0;

	// Summing all variables
	for (int i = 0; i < varcount; i++) {
		sum += variables[i];
	}

	return sum;
}

int product(int varcount, int variables[]) {
	// Initializing default product value
	int product = 1;

	// Multiplying all variables
	for (int i = 0; i < varcount; i++) {
		product *= variables[i];
	}

	return product;
}

float average(int varcount, int variables[]) {
	// Getting sum of the variables with the help of the sum function
	int varsum = sum(varcount, variables);

	// Getting the average value by dividing the variable sum to variable count and returning it
	return (float)varsum / (float)varcount;
}

int smallestNum(int varcount, int variables[]) {
	// Initializing the first variable as the smallest
	int smallest = variables[0];

	// Doing the comparisons for the smallest number
	for (int i = 1; i < varcount; i++) {
		if (variables[i] < smallest) {
			smallest = variables[i];
		}
	}

	return smallest;
}

void outputs(int sum, int product, float avg, int smallest) {
	cout << "Sum is " << sum << endl;
	cout << "Product is " << product << endl;
	cout << "Average is " << avg << endl;
	cout << "Smallest is " << smallest << endl;
}

void errors(int varcount, int counter) {

	if (counter != varcount) {
		cout << "Your variable count is wrong." << endl;
		exit(1);
	}
}